# Virtual Radio Telescope

A educational project implementing a simple virtual radio telescope to
illustrate the data pipeline: from a virtual dish's signal, through
various data processors, to a final image.

The intention is to start extremely simply (single dish, clean signal)
and add complexity bit-by-bit (e.g. interferometry, interference,
calibration) in a modular way so that newcomers can build up their
understanding incrementally. As such, dependencies are kept to a minimum
and algorithms are reimplemented where it is feasible. Additionally
readability & understandability is a priority over performance (within
reason). (Future) documentation will aim to be comprehensive and
comprehensible.

I have started this project in order to help myself properly understand
the intricacies of radio astronomy data processing as a Software
Engineer working on the [Square Kilometre Array](https://www.skao.int/)
without the complexities of big-data and the complexities associated
with that (e.g. distributed systems). I have found plenty of high level
descriptions of the concepts behind radio-astronomy (with lots of
maths!), much of which assumes certain knowledge. Here we will assume
some programming knowledge for high level data flow, and introduce
concepts as needed. Nevertheless, some topics & resources that will be
useful to be familiar with:

* Fourier transform
  * [But what is the Fourier Transform? A visual introduction.](https://www.youtube.com/watch?v=spUNpyF58BY)
  * [But what is a Fourier series? From heat flow to drawing with circles](https://www.youtube.com/watch?v=r6sGWTCMz2k)
* Linear Algebra
  * [Essence of linear algebra](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)
* Introduction to Radio Astronomy & Interferometry
  * [ANITA Lecture - Radio Astronomy and Interferometry Fundamentals – David Wilner](https://www.youtube.com/watch?v=0TwnZhiEc3A)
  * [ANITA Lecture - Imaging and Deconvolution in Radio Interferometry – David Wilner](https://www.youtube.com/watch?v=mRUZ9eckHZg)

## High level overview

The first pass of the data pipeline to work will be provide a single
source which will be imaged using a single dish to produce an image:

![theoretical perfect radio telescope](./dataflow.drawio.svg)